package pl.sdacademy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * http://dominisz.pl
 * 23.09.2017
 */

//http://localhost:8888/google/search?query=zapytanie&category=obrazki
@WebServlet(value = "/search")
public class GoogleServlet extends HttpServlet {

    private String nullToEmpty(String text){
        return text == null ? "": text;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String query = nullToEmpty(req.getParameter("query"));
        String category = nullToEmpty(req.getParameter("category"));

        out.println("<h1>wielka wyszukiwarka</h1>");
        out.println("Zapytanie: " + query + "<br>");
        out.println("Kategoria: " + category + "<br>");

        Enumeration<String> allParameters = req.getParameterNames();
        while (allParameters.hasMoreElements()){
            String name = allParameters.nextElement();
            String value = req.getParameter(name);
            out.println("Parametr: " + name + ", wartosc: " + value + "<br>");
        }

    }

}
